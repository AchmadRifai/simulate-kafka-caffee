package achmad.rifai.kitchen.exceptions;

public class BreakProcessException extends RuntimeException {

	private static final long serialVersionUID = 6045255792239711747L;

	public BreakProcessException(String message) {
		super(message);
	}

}
