# Pesan Menu Caffee Microservices

## How To Run

### Linux

`docker-compose up -d`

### Windows

`docker-compose up -d`

### Mac

`docker-compose up -d`

## Tech Stack

1. Java
2. Docker
3. PostgreSQL
4. Apache Kafka
5. Redis
6. ElasticSearch